package httpexample.potingchiang.com.servertalk_2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.net.HttpURLConnection;

import httpexample.potingchiang.com.mylibrary.FileUpload;
import httpexample.potingchiang.com.mylibrary.GetJsonFromServer;

public class MainActivity extends AppCompatActivity {

    //declaration
    //connection
    private final static String mUrl = "http://192.168.0.52:8886/POSTest.php";
//    private GetJsonFromServer getJsonMrg;
    //layout elements
    private TextView msg;
    private ImageButton getMsg;
    private String mJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //init. layout elements
        msg = (TextView) findViewById(R.id.msg);
        getMsg = (ImageButton) findViewById(R.id.getMsg);

//        //init my async task
//        getJsonMrg = new GetJsonFromServer(MainActivity.this);
//        //setup attributes
//        getJsonMrg.setMsg(msg);

        //set onclick listener
        getMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //solution 1: //only one task be instance, so only can be executed once
//                //execute async. task
//                if (getJsonMrg.getStatus() != AsyncTask.Status.RUNNING) {
//
//                    //execute task
//                    getJsonMrg.execute(mUrl);
//                    //update mJson
//                    //useless
//                    mJson = getJsonMrg.getmJson();
//
//                    //message
//                    Toast.makeText(MainActivity.this, getJsonMrg.getStatus().toString(), Toast.LENGTH_SHORT).show();
//
//                }
//                else {
//
//                    //message
//                    Toast.makeText(MainActivity.this, "The task has been executed already!", Toast.LENGTH_SHORT).show();
//                }

                //solution 2: each time call this function, it make a new instance/task for the task
                //so it is seen as a new task each time, it can be executed as many times as u want
                getJson(mUrl);
            }
        });
    }

    //other methods
    private void getJson(String url) {

        //init. get json from server
        GetJsonFromServer getJsonFromServer = new GetJsonFromServer(MainActivity.this);
        getJsonFromServer.setMsg(msg);
        //execute
        getJsonFromServer.execute(url);
        //update mJson
        mJson = msg.getText().toString();
        //message
        Toast.makeText(
                MainActivity.this, "Status: "
                + getJsonFromServer.getStatus().toString() +
                "\nData Info: " + mJson,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
